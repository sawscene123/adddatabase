package com.example.saw.adddatabase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by saw on 12/21/2017.
 */

public class CustomStudentAdapter extends BaseAdapter {
    private List<StudentDto> studentDtoList;
    private Context context;
    private int lastPosition = -1;

    public CustomStudentAdapter(Context context, List<StudentDto> studentDtoList) {
        this.context = context;
        this.studentDtoList = studentDtoList;
    }

    @Override
    public int getCount() {
        return studentDtoList.size();
    }

    @Override
    public StudentDto getItem(int position) {
        return studentDtoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private TextView tvName, tvRollNo, tvMarks;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.row_student_detail, parent, false);
            holder = new ViewHolder();
            holder.tvName = convertView.findViewById(R.id.tvName);
            holder.tvRollNo = convertView.findViewById(R.id.tvRollNo);
            holder.tvMarks = convertView.findViewById(R.id.tvMarks);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        StudentDto studentDto = getItem(position);

        holder.tvName.setText(studentDto.getName());
        holder.tvRollNo.setText(String.valueOf(studentDto.getRollNo()));
        holder.tvMarks.setText(String.valueOf(studentDto.getMarks()));
        if (lastPosition < position) {
            /*Animation scaleOutAnim = AnimationUtils.loadAnimation(context, R.anim.scale_out);
            holder.tvRollNo.startAnimation(scaleOutAnim);
            Animation slideFromRightAnim = AnimationUtils.loadAnimation(context, R.anim.slide_from_right);
            holder.tvName.startAnimation(slideFromRightAnim);*/
            Animation slideFromRightAnim = AnimationUtils.loadAnimation(context, R.anim.slide_from_right);
            convertView.startAnimation(slideFromRightAnim);
        }
        lastPosition = position;
        return convertView;
    }
}
