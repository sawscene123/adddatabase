package com.example.saw.adddatabase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by saw on 12/20/2017.
 */

public class AddStudentActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUsername, etRoll, etMarks;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        initUi();
    }

    private void initUi() {
        etUsername = findViewById(R.id.et_username);
        etRoll = findViewById(R.id.et_roll);
        etMarks = findViewById(R.id.et_marks);
        findViewById(R.id.btn_add).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                if (allFieldValid()) {
                    String username = etUsername.getText().toString();
                    int rollNo = Integer.parseInt(etRoll.getText().toString());
                    double marks = Double.parseDouble(etMarks.getText().toString());

                    StudentDto studentDto = new StudentDto(username, rollNo, marks);
                    StudentDao studentDao = new StudentDao(this);
                    studentDao.insert(studentDto);
                }
                break;
        }
    }

    private boolean allFieldValid() {
        String username = etUsername.getText().toString();
        if (TextUtils.isEmpty(username)) {
            Toast.makeText(this, "Username is empty.", Toast.LENGTH_SHORT).show();
            return false;
        }

        String roll = etRoll.getText().toString();
        if (TextUtils.isEmpty(roll)) {
            Toast.makeText(this, "Roll is empty", Toast.LENGTH_SHORT).show();
            return false;
        }

        String marks = etMarks.getText().toString();

        if (TextUtils.isEmpty(marks)) {
            Toast.makeText(this, "Marks is empty", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            double m = Double.parseDouble(marks);
            if (m < 0 || m > 100) {
                Toast.makeText(this, "Marks must lie between 0 and 100", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }
}
