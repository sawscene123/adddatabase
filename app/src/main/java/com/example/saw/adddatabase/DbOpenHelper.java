package com.example.saw.adddatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by saw on 12/20/2017.
 */

public class DbOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "sutdent_db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_STUDENT = "tbl_student";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "_name";
    public static final String COLUMN_ROLL_NO = "_roll_no";
    public static final String COLUMN_MARKS = "_marks";

    public DbOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table tbl_student _id integer primary key autoincrement,
        // _name text, _roll_no integer, _marks real
        db.execSQL("CREATE TABLE " + TABLE_STUDENT + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_ROLL_NO + " INTEGER, " +
                COLUMN_MARKS + " REAL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_STUDENT);
        onCreate(db);
    }
}
