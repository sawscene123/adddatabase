package com.example.saw.adddatabase;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by saw on 12/21/2017.
 */

public class ViewStudentListActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student);
        final StudentDao studentDao = new StudentDao(this);
        final List<StudentDto> studentDtoList = studentDao.getAllStudent();

        GridView lvStudents = findViewById(R.id.lv_students);

        /*final String[] nameArrays = new String[studentDtoList.size()];

        for (int i = 0; i < studentDtoList.size(); i++) {
            StudentDto studentDto = studentDtoList.get(i);
            nameArrays[i] = studentDto.getName();
        }

        String[] rollArray = new String[studentDtoList.size()];
        for(int i = 0; i < studentDtoList.size(); i++){
            rollArray[i] = String.valueOf(studentDtoList.get(i).getRollNo());
        }

        final ArrayAdapter nameAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
        nameArrays);//Ram, Shyam, Sita, Gita
        lvStudents.setAdapter(nameAdapter);
        lvStudents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ViewStudentListActivity.this, position +" : " + nameArrays[position], Toast.LENGTH_SHORT)
                        .show();

                StudentDto studentDto = studentDtoList.get(position);
                studentDao.delete(studentDto);
                nameArrays[position] = "";
                nameAdapter.notifyDataSetChanged();
            }
        });*/

        final CustomStudentAdapter adapter = new CustomStudentAdapter(this, studentDtoList);
        lvStudents.setAdapter(adapter);
        lvStudents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*StudentDto studentDto = ((CustomStudentAdapter)
                parent.getAdapter()).getItem(position);// studentDtoLis.get(position);
                studentDao.delete(studentDto);
                studentDtoList.remove(position);
                adapter.notifyD(ataSetChanged();*/
            }
        });

        lvStudents.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                Toast.makeText(ViewStudentListActivity.this,
                        "Long Clicked on " + position, Toast.LENGTH_SHORT).show();
                new AlertDialog.Builder(ViewStudentListActivity.this)
                        .setTitle("Select Option")
                        .setItems(new String[]{"Delete", "Edit"}, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                switch (which) {
                                    case 0:
                                        //Delete action
                                        StudentDto studentDto = ((CustomStudentAdapter)
                                                parent.getAdapter()).getItem(position);// studentDtoLis.get(position);
//                                        studentDto.setId(-1);
                                        int affectedRows = studentDao.delete(studentDto);
                                        if (affectedRows > 0) {
                                            Toast.makeText(ViewStudentListActivity.this,
                                                    "Deleted", Toast.LENGTH_SHORT).show();
                                            studentDtoList.remove(position);
                                            adapter.notifyDataSetChanged();
                                        } else {
                                            Toast.makeText(ViewStudentListActivity.this,
                                                    "Data is not deleted", Toast.LENGTH_SHORT).show();
                                        }
                                        break;
                                    case 1:
                                        //Edit Action
                                        break;
                                }
                            }
                        })
                        .create()
                        .show();
                return true;
            }
        });
    }
}
