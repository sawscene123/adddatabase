package com.example.saw.adddatabase;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by saw on 1/2/2018.
 */

public class ItemSpaceDecoration extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = 8;
        } else {
            outRect.top = 4;
        }
        outRect.left = 8;
        outRect.right = 8;
        if (parent.getChildAdapterPosition(view) == (parent.getAdapter().getItemCount() - 1)) {
            outRect.bottom = 8;
        } else {
            outRect.bottom = 4;
        }
    }
}
