package com.example.saw.adddatabase;

/**
 * Created by saw on 12/20/2017.
 */

public class StudentDto {
    private int id;
    private String name;
    private int rollNo;
    private double marks;

    public StudentDto(String username, int rollNo, double marks) {
        this.name = username;
        this.rollNo = rollNo;
        this.marks = marks;
    }

    public StudentDto(int id, String name, int rollNo, double marks) {
        this.id = id;
        this.name = name;
        this.rollNo = rollNo;
        this.marks = marks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public double getMarks() {
        return marks;
    }

    public void setMarks(double marks) {
        this.marks = marks;
    }
}
