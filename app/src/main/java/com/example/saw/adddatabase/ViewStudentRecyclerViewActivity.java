package com.example.saw.adddatabase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by saw on 1/2/2018.
 */

public class ViewStudentRecyclerViewActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student_recycler_view);
        RecyclerView rvStudent = findViewById(R.id.rvStudent);
        rvStudent.addItemDecoration(new ItemSpaceDecoration());
        final StudentDao studentDao = new StudentDao(this);
        final List<StudentDto> studentDtoList = studentDao.getAllStudent();
        rvStudent.setLayoutManager(new LinearLayoutManager(this));
        CustomStudentRvAdapter adapter = new CustomStudentRvAdapter(this, studentDtoList);
        rvStudent.setAdapter(adapter);
    }
}
