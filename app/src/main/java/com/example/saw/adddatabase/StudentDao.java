package com.example.saw.adddatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saw on 12/20/2017.
 */

public class StudentDao {
    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase database;
    private String[] allColumns = new String[]{
            DbOpenHelper.COLUMN_ID,
            DbOpenHelper.COLUMN_NAME,
            DbOpenHelper.COLUMN_ROLL_NO,
            DbOpenHelper.COLUMN_MARKS
    };

    public StudentDao(Context context) {
        dbOpenHelper = new DbOpenHelper(context);
    }

    public void insert(StudentDto studentDto) {
        database = dbOpenHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DbOpenHelper.COLUMN_NAME, studentDto.getName());
        values.put(DbOpenHelper.COLUMN_ROLL_NO, studentDto.getRollNo());
        values.put(DbOpenHelper.COLUMN_MARKS, studentDto.getMarks());

        try {
            //insert into table_name (name, roll, marks) values ("ram", 1, 100)
            database.insert(DbOpenHelper.TABLE_STUDENT, null, values);
        } finally {
            database.close();
            dbOpenHelper.close();
        }
    }

    public List<StudentDto> getAllStudent() {
        database = dbOpenHelper.getReadableDatabase();
        List<StudentDto> studentDtoList = new ArrayList<>();
        //select * from table_student
        Cursor cursor = database.query(DbOpenHelper.TABLE_STUDENT, allColumns, null, null, null, null, null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();

                while (!cursor.isAfterLast()) {
                    int idIndex = cursor.getColumnIndex(DbOpenHelper.COLUMN_ID); // 0
                    int id = cursor.getInt(idIndex);
                    double marks = cursor.getDouble(cursor.getColumnIndex(DbOpenHelper.COLUMN_MARKS));
                    String name = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_NAME));
                    int rollNo = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_ROLL_NO));

                    StudentDto studentDto = new StudentDto(id, name, rollNo, marks);
                    studentDtoList.add(studentDto);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        database.close();
        dbOpenHelper.close();
        return studentDtoList;
    }

    public int delete(StudentDto studentDto) {
        database = dbOpenHelper.getWritableDatabase();

        int affectedRows;
        try {
            //delete from table_student where _id=studentDto.getId()
            affectedRows = database.delete(DbOpenHelper.TABLE_STUDENT,
                    DbOpenHelper.COLUMN_ID + "=?", new String[]{String.valueOf(studentDto.getId())});

        } finally {
            database.close();
            dbOpenHelper.close();
        }
        return affectedRows;
    }

}
