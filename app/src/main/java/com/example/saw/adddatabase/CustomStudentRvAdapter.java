package com.example.saw.adddatabase;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by saw on 1/2/2018.
 */

public class CustomStudentRvAdapter extends RecyclerView.Adapter<CustomStudentRvAdapter.ViewHolder> {
    private Context context;
    private List<StudentDto> studentDtoList;

    public CustomStudentRvAdapter(Context context, List<StudentDto> studentDtoList) {
        this.context = context;
        this.studentDtoList = studentDtoList;
    }

    @Override
    public int getItemCount() {
        return studentDtoList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.row_student_detail, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        StudentDto studentDto = studentDtoList.get(position);
        holder.tvName.setText(studentDto.getName());
        holder.tvRollNo.setText(String.valueOf(studentDto.getRollNo()));
        holder.tvMarks.setText(String.valueOf(studentDto.getMarks()));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvRollNo, tvMarks;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvRollNo = itemView.findViewById(R.id.tvRollNo);
            tvMarks = itemView.findViewById(R.id.tvMarks);
        }
    }
}
